from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from ads.serializers.comment import CommentSerializer
from ads.serializers.ad import AdSerializer, AdDetailSerializer

from ads.models import Ad, Comment

from ads.serializers.spectacular import CommonDetailSerializer, CommonDetailAndStatusSerializer

from ads.permissions import IsOwner, IsAdmin


@extend_schema(tags=["Comment"])
@extend_schema(
    summary="Конечная точка API для просмотра списка комментариев.",
    responses={
        status.HTTP_200_OK: CommentSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
        status.HTTP_405_METHOD_NOT_ALLOWED: CommonDetailSerializer,
    }
)
class ComSerializeListAPIView(ListAPIView):
    """
    Просмотр списка комментариев.
    Доступно аутентифицированным пользователям.
    """
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """
        Получение комментария с фильтрацией по идентификатору.
        """
        return Comment.objects.filter(ad=self.kwargs.get('pk'))


@extend_schema(tags=["Comment"])
@extend_schema(
    summary="Конечная точка API для просмотра подробностей комментария.",
    responses={
        status.HTTP_200_OK: CommentSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
        status.HTTP_405_METHOD_NOT_ALLOWED: CommonDetailSerializer,
    }
)
class ComSerializeRetrieveAPIView(RetrieveAPIView):
    """
    Просмотр подробностей комментария.
    Доступно аутентифицированным пользователям.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    lookup_url_kwarg = 'commentId'
    permission_classes = [IsAuthenticated]


@extend_schema(tags=["Comment"])
@extend_schema(
    summary="Конечная точка API для создания комментария.",
    responses={
        status.HTTP_201_CREATED: CommentSerializer,
        status.HTTP_400_BAD_REQUEST: CommonDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_403_FORBIDDEN: CommonDetailAndStatusSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class ComSerializeCreateAPIView(CreateAPIView):
    """
    Создание комментария. Доступно всем аутентифицированным пользователям.
    Доступно аутентифицированным пользователям.
    """
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        """
        Создание комментария к текущему объявлению с присвоением автора текущему пользователю.
        """
        comment = serializer.save()
        comment.author = self.request.user
        comment.ad = Ad.objects.get(id=self.kwargs.get('pk'))
        comment.save()


@extend_schema(tags=["Comment"])
@extend_schema(
    summary="Конечная точка API для редактирования комментария.",
    responses={
        status.HTTP_200_OK: CommentSerializer,
        status.HTTP_400_BAD_REQUEST: CommonDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class ComSerializeUpdateAPIView(UpdateAPIView):
    """
    Редактирование комментария.
    Доступно аутентифицированным авторам (владельцам) и администратору.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    lookup_url_kwarg = 'commentId'
    permission_classes = [IsAuthenticated, IsOwner | IsAdmin]


@extend_schema(tags=["Comment"])
@extend_schema(
    summary="Конечная точка API для удаления комментария.",
    responses={
        status.HTTP_201_CREATED: AdDetailSerializer,
        status.HTTP_400_BAD_REQUEST: CommonDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_403_FORBIDDEN: CommonDetailAndStatusSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class ComSerializeDestroyAPIView(DestroyAPIView):
    """
    Удаление комментария.
    Доступно аутентифицированным авторам (владельцам) и администратору.
    """
    queryset = Comment.objects.all()
    lookup_url_kwarg = 'commentId'
    permission_classes = [IsAuthenticated, IsOwner | IsAdmin]
