from django.db import models
from django.utils import timezone

from users.models import User

NULLABLE = {'blank': True, 'null': True}


class Ad(models.Model):
    """
    Модель объявления
    """
    author = models.ForeignKey(User, **NULLABLE, on_delete=models.CASCADE, verbose_name='автор')
    title = models.CharField(max_length=200, verbose_name='название')
    description = models.TextField(max_length=1000, **NULLABLE, verbose_name='описание')
    price = models.PositiveIntegerField(verbose_name='цена')
    image = models.ImageField(upload_to='ad/%Y-%m-%d/', **NULLABLE, verbose_name='фотография')
    created_at = models.DateTimeField(default=timezone.now, verbose_name='добавлено')

    class Meta:
        verbose_name = 'объявление'
        verbose_name_plural = 'объявления'
        ordering = ['-created_at']

    def __str__(self):
        return self.title


class Comment(models.Model):
    """
    Модель комментариев
    """
    author = models.ForeignKey(User, **NULLABLE, on_delete=models.CASCADE, verbose_name='автор')
    ad = models.ForeignKey(Ad, **NULLABLE, on_delete=models.CASCADE, verbose_name='объявление')
    text = models.TextField(max_length=1000, verbose_name='комментарий')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='добавлен')

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'
        ordering = ['-created_at']

    def __str__(self):
        return self.text

