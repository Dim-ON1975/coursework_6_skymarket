from djoser.serializers import UserSerializer as BaseUserSerializer, \
    UserCreateSerializer as BaseUserRegistrationSerializer, UserCreatePasswordRetypeSerializer
from django.contrib.auth import authenticate, get_user_model
from djoser.conf import settings
from djoser.serializers import TokenCreateSerializer

User = get_user_model()


class UserRegistrationSerializer(BaseUserRegistrationSerializer):
    """ Сериализация регистрации пользователя """
    class Meta(BaseUserRegistrationSerializer.Meta):
        fields = '__all__'


class CurrentUserSerializer(BaseUserSerializer):
    """ Сериализация текущего пользователя """
    class Meta(BaseUserSerializer.Meta):
        fields = ('id', 'first_name', 'last_name', 'phone', 'email', 'image', 'role',)
