from rest_framework import serializers

from users.serializers import CurrentUserSerializer

from ads.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """
    Сериализатор для комментариев.
    """
    # Сведения об авторе (поле для сериализатора)
    author = CurrentUserSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'
