import django_filters
from .models import Ad


class AdTitleFilter(django_filters.FilterSet):
    """
    Пользовательский фильтр для поиска совпадений в заголовке объявления.
    """
    # Поиск по полю 'title' с учётом нечувствительного к регистру содержания подстроки.
    title = django_filters.CharFilter(field_name="title", lookup_expr="icontains",)

    class Meta:
        model = Ad
        fields = ("title",)
