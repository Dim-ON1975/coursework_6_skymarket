from rest_framework import serializers

from ads.models import Ad

from users.serializers import CurrentUserSerializer


class AdSerializer(serializers.ModelSerializer):
    """
    Сериализатор для объявления.
    """
    author_first_name = serializers.SerializerMethodField()
    author_last_name = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    def get_author_first_name(self, obj):
        """ Имя автора """
        return obj.author.first_name

    def get_author_last_name(self, obj):
        """ Фамилия автора """
        return obj.author.last_name

    def get_phone(self, obj):
        """ Номер телефона автора """
        return str(obj.author.phone)

    class Meta:
        model = Ad
        fields = '__all__'


class AdDetailSerializer(serializers.ModelSerializer):
    """
    Сериализатор для детализации объявления.
    """
    # Сведения об авторе (поле для сериализатора)
    author = CurrentUserSerializer(read_only=True)

    class Meta:
        model = Ad
        fields = '__all__'
