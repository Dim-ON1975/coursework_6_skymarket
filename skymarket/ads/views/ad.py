from django_filters.rest_framework import DjangoFilterBackend
from ads.filters import AdTitleFilter
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny

from ads.serializers.ad import AdSerializer, AdDetailSerializer

from ads.models import Ad

from ads.serializers.spectacular import CommonDetailSerializer, CommonDetailAndStatusSerializer

from ads.pagination import AdPagination

from ads.permissions import IsOwner, IsAdmin


@extend_schema(tags=["Ad"])
@extend_schema(
    summary="Конечная точка API для просмотра списка объявлений.",
    responses={
        status.HTTP_200_OK: AdSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
        status.HTTP_405_METHOD_NOT_ALLOWED: CommonDetailSerializer,
    }
)
class AdSerializeListAPIView(ListAPIView):
    """
    Просмотр списка объявлений. Могут просматривать все.
    """
    serializer_class = AdSerializer
    pagination_class = AdPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = AdTitleFilter
    permission_classes = [AllowAny]

    def get_queryset(self):
        """
        Возвращает набор запросов с фильтром (объявления пользователя),
        если в URL-адресе указано «me».
        В противном случае возвращает все объявления.
        """
        if 'me' in self.request.path:
            return Ad.objects.filter(author=self.request.user)
        else:
            return Ad.objects.all()


@extend_schema(tags=["Ad"])
@extend_schema(
    summary="Конечная точка API для просмотра подробностей объявления.",
    responses={
        status.HTTP_200_OK: AdDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
        status.HTTP_405_METHOD_NOT_ALLOWED: CommonDetailSerializer,
    }
)
class AdSerializeRetrieveAPIView(RetrieveAPIView):
    """
    Детализация объявления.
    Просматривать может только аутентифицированный пользователь.
    """
    queryset = Ad.objects.all()
    serializer_class = AdDetailSerializer
    permission_classes = [IsAuthenticated]


@extend_schema(tags=["Ad"])
@extend_schema(
    summary="Конечная точка API для создания объявления.",
    responses={
        status.HTTP_201_CREATED: AdSerializer,
        status.HTTP_400_BAD_REQUEST: CommonDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_403_FORBIDDEN: CommonDetailAndStatusSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class AdSerializeCreateAPIView(CreateAPIView):
    """
    Создание и сохранение объявления.
    """
    serializer_class = AdSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        """
        Создание объявления с автором, закрепленным за текущим пользователем.
        Создавать может только аутентифицированный пользователь.
        """
        ad = serializer.save()
        ad.author = self.request.user
        ad.save()


@extend_schema(tags=["Ad"])
@extend_schema(
    summary="Конечная точка API для обновления объявления.",
    responses={
        status.HTTP_200_OK: AdDetailSerializer,
        status.HTTP_400_BAD_REQUEST: CommonDetailSerializer,
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class AdSerializeUpdateAPIView(UpdateAPIView):
    """
    Обновление объявления.
    Изменить могут только аутентифицированные авторы (владельцы) или администратор.
    """
    queryset = Ad.objects.all()
    serializer_class = AdDetailSerializer
    permission_classes = [IsAuthenticated, IsOwner | IsAdmin]


@extend_schema(tags=["Ad"])
@extend_schema(
    summary="Конечная точка API для удаления объявления.",
    responses={
        status.HTTP_204_NO_CONTENT: '',
        status.HTTP_401_UNAUTHORIZED: CommonDetailSerializer,
        status.HTTP_403_FORBIDDEN: CommonDetailAndStatusSerializer,
        status.HTTP_404_NOT_FOUND: CommonDetailSerializer,
    }
)
class AdSerializeDestroyAPIView(DestroyAPIView):
    """
    Удаление объявления.
    Удалить могут только аутентифицированные авторы (владельцы) или администратор.
    """
    queryset = Ad.objects.all()
    permission_classes = [IsAuthenticated, IsOwner | IsAdmin]
