from rest_framework.test import APITestCase
from rest_framework import status
from users.models import User


class UserTestCase(APITestCase):

    def test_create_user(self):
        """ Тестирование создания пользователя """

        data = {
            "first_name": "user_fn",
            "last_name": "user_ln",
            "phone": '+79198783434',
            "email": 'test@test.ru',
            "password": '123qwe456asd',
            "role": 'user',
        }

        response = self.client.post('/users/', data=data)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED,
        )

        self.assertEqual(
            response.json().get('action'),
            data.get('action')
        )

        # Проверяем наличие записи в базе данных
        self.assertTrue(
            User.objects.all().exists()
        )

    def test_create_superuser(self):
        """ Тестирование создания администратора """

        data = {
            "first_name": "user_admin_fn",
            "last_name": "user_admin_ln",
            "phone": '+79198784545',
            "email": 'test_admin@test.ru',
            "password": '123qwe456asd',
            "role": 'admin',
        }

        response = self.client.post('/users/', data=data)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED,
        )

        self.assertEqual(
            response.json().get('action'),
            data.get('action')
        )

        # Проверяем наличие записи в базе данных
        self.assertTrue(
            User.objects.all().exists()
        )
