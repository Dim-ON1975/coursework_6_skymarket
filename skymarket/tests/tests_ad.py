from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from users.models import User
from ads.models import Ad, Comment


class AdTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create(email='user@test.ru')
        self.user_admin = User.objects.create(email='user_admin@test.ru', role='admin')
        self.image = SimpleUploadedFile("test_ad.webp", b"file_content", content_type="image/webp")
        self.ad = Ad.objects.create(
            title="title_ad",
            description="description_ad",
            price=1000,
            image=self.image,
            author=self.user
        )
        self.comment = Comment.objects.create(
            text="text_comment",
            author=self.user,
            ad=self.ad
        )

        # Аутентификация пользователя
        self.client.force_authenticate(user=self.user)

    def test_user(self):
        """ Тестирование роли пользователя """

        self.assertTrue(
            self.user.is_active,
            True
        )

        self.assertTrue(
            self.user.__str__(),
            "user@test.ru"
        )

        self.assertEqual(
            self.user.is_admin,
            False
        )

        self.assertEqual(
            self.user.is_staff,
            False
        )

        self.assertEqual(
            self.user.is_user,
            True
        )

        self.assertEqual(
            self.user.is_superuser,
            False
        )

        self.assertEqual(
            self.user is self.user.has_perm,
            False
        )

        self.assertEqual(
            self.user is self.user.has_module_perms,
            False
        )

    def test_create_ad(self):
        """ Тестирование создания объявления """

        data = {
            "title": "title_ad",
            "description": "description_ad",
            "price": 1000
        }

        ad_create_url = reverse('ads:ads_create')
        response = self.client.post(ad_create_url, data=data)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED,
        )

        self.assertEqual(
            response.json().get('action'),
            data.get('action')
        )

        self.assertTrue(
            Ad.objects.get(pk=self.ad.pk).title,
            data.get('action')
        )

        # Проверяем наличие записи в базе данных
        self.assertTrue(
            Ad.objects.all().exists()
        )

        self.assertEqual(
            Ad.objects.get(pk=self.ad.pk).title,
            str(self.ad)
        )

    def test_list_ad(self):
        """ Тестирование чтения списка объявлений """

        ad_list_url = reverse('ads:ads_all_list')

        response = self.client.get(ad_list_url)

        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )

        self.assertEqual(
            Ad.objects.get(pk=self.ad.pk).title,
            response.json().get('results')[0].get('title'))

    def test_list_ad_me(self):
        """ Тестирование чтения списка личных объявлений """

        ad_list_me_url = reverse('ads:ads_me_list')

        response = self.client.get(ad_list_me_url)

        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )

        self.assertEqual(
            Ad.objects.get(pk=self.ad.pk).title,
            response.json().get('results')[0].get('title'))

    def test_retrieve_ad(self):
        """ Тест чтения одного объявления """

        ad_one_url = reverse('ads:ads_detail', args=[self.ad.pk])

        response = self.client.get(ad_one_url)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
        )

        response = response.json()

        self.assertEqual(response.get('author')['id'], self.user.pk)
        self.assertEqual(response.get('title'), 'title_ad')
        self.assertEqual(response.get('description'), "description_ad")
        self.assertEqual(response.get('price'), 1000)

    def test_update_ad(self):
        """ Тест редактирования объявления """

        data = {
            "title": "title_update_ad",
            "description": "description_update_ad",
            "price": 2000
        }

        ad_update_url = reverse('ads:ads_update', args=[self.ad.pk])

        response = self.client.patch(ad_update_url, data)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
        )
        response = response.json()

        self.assertEqual(response.get('author')['id'], self.user.pk)
        self.assertEqual(response.get('title'), 'title_update_ad')
        self.assertEqual(response.get('description'), "description_update_ad")
        self.assertEqual(response.get('price'), 2000)

    def test_delete_ad(self):
        """ Тест удаления объявления """

        ad_delete_url = reverse('ads:ads_delete', args=[self.ad.pk])

        response = self.client.delete(ad_delete_url)

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT,
        )
        self.assertFalse(
            Ad.objects.all().exists(),
        )
