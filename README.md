# Дипломный проект
## Доска объявлений
### Заберов Дмитрий (zaberov.dv@internet.ru), Python IND 27.0

### Сайт
Убедиться в работоспособности сайта, развёрнутого на локальном компьютере посредством Docker:
 - frontend: http://localhost:3000/
 - backend:
   - административная панель: http://localhost:8001/admin/
   - документация swagger (OpenAPI 2): http://localhost:8001/api/swagger/
   - документация specicular (OpenAPI 3):  http://localhost:8001/api/docs/
   - документация redoc: http://localhost:8001/api/redoc/

## Технологии
Backend разработан на ЯП Python 3.11 с использованием библиотек-фреймворков Django, Django REST framework, drf-spectacular.
Frontend разработан на языках JavaScript, SCSS, HTML с использованием библиотеки-фреймворка React.js

## Использование
Сайт объявлений, на котором пользователи могут выставлять и продавать свои товары. Имеет интуитивно понятный и лаконичный интерфейс, системы объявлений и отзывов.
![indexhtml.png](image_readme/indexhtml.png)

## Тестирование
1. Настройки в файле **skymarket/.coveragerc**
2. Из директории проекта **skymarket** запустить команду тестирования `python manage.py test`
3. После успешного прохождения тестов запустить следующие команды для формирования отчёта(-ов):
    - запуск тестов конкретных приложений с выводом подробностей тестирования: `coverage run --source='ads,users'  manage.py test -v 2`
    - вывод результатов тестирования в виде отчёта в терминал: `coverage report --skip-covered`
    
    
<p align="center"><img src="image_readme/cov_report.png" width="500"></p>
    - вывод результатов тестирования в виде html-отчёта: `coverage html --skip-covered`
    
    
<p align="center"><img src="image_readme/cov_html.png" width="500"></p>


### **Установка**
Для установки проекта, следуйте инструкциям, указанным ниже:

1. Сделайте Fork этого репозитория. Клонируйте форкнутый репозиторий, чтобы получить его локально.

2. Перейдите в директорию **skymarket** с backend-частью проекта.

3. Создайте и активируйте виртуальное окружение:</p>

   `poetry init`
   
   `poetry shell`

4. Установите зависимости проекта (зависимости, необходимые для установки, можно найти в файле **pyproject.toml**):

   `poetry install`

5. Создайте файл **.env** в корневой папке проекта (**/skymarket/**) и заполните данные для настройки проекта из файла **.env.sample**
6. В директории **/skymarket/** создайте дерево каталогов **persistentdata/**, как показано на иллюстрации ниже:

<p align="center"><img src="image_readme/persistentdata.png" width="200"></p>

Все остальные настройки указаны в **frontend_react/Dockerfile**, **skymarket/Dockerfile**, **market_postgres/docker-compose.yaml**, **market_postgres/default.conf**. Для запуска проекта достаточно запустить **docker-compose.yaml** из директории **market_postgres/**, о чём написано ниже.

### Требования
Для запуска проекта в Docker необходимо:
- выполнить команду `docker-compose up --build`

_или_
- последовательно выполнить команды:
  - собрать образ: `docker-compose build`
  - запустить контейнеры: `docker-compose up`

### Дополнительно
Следует учитывать, что настройки вынесены в файл .env, который не выкладывается в репозиторий, однако имеется файл **.env.sample**, позволяющий разработчику определить перечень необходимых ключей и настроек для работы приложений.

Полезные команды для перезапуска процесса контейнеризации:
- Очистка тома: `docker volume prune -f`
- Удалить всё и сразу без подтверждений: `docker system prune -af`

Полезные команды для получения доступа к контейнеру, содержащему проект:
- Запуск командной оболочки: `docker-compose exec app bash`
- Активация виртуального окружения в контейнере: `source env/bin/activate`

## Особенности
### Проект содержит несколько приложений:
1.  users – приложение для регистрации и авторизации пользователей проекта посредством Djoser (информацию по **users.urls** см. ниже).
2.  ads – приложение для объявлений и комментариев к ним, включающие операции создания, просмотра, редактирования и удаления (CRUD).

### Соответствие ТЗ:
1. Для разных сервисов созданы отдельные контейнеры (db - postgresql, backend - django, frontend - react.js).
2. Бэкенд-часть проекта имеет следующий функционал:
   - Авторизация и аутентификация пользователей посредством Djoser. Письмо для сброса пароля приходит на указанную почту со ссылкой для сброса пароля.
   ![email_reset_password.png](image_readme/email_reset_password.png)
   - Профиль пользователя:
   ![user_profile_1.png](image_readme/user_profile_1.png)
   ![user_profile_2.png](image_readme/user_profile_2.png)
   - Распределение ролей между пользователями (пользователь и админ), для чего используются необходимые permissions.
   - Реализовано восстановление пароля через электронную почту.
   - CRUD для объявлений на сайте (админ может удалять или редактировать все объявления, а пользователи только свои). Под каждым объявлением пользователи могут оставлять отзывы.
   ![ads.png](image_readme/ads.png)
   - В заголовке сайта можно осуществлять поиск объявлений по названию.
   ![search.png](image_readme/search.png)

### Установка зависимостей
Зависимости, необходимые для работы и тестирования проекта указаны в **skymarket/pyproject.toml** и **frontend_react/package.json**.

### Справочные данные по users.urls

1. **POST jwt/create/?** [name='jwt-create'] - получение refresh и access токенов jwt
   
         запрос: email, password.
            
         результат: refresh и access токены jwt, например:
         
         {
             "refresh": "eyJhbGciOiJIUzI1NiIs...2E3MiIsInVzZXJfaWQiOjF9.DPUiIbzKHRYiscM0dxGxryox_6YwYV899eKu0jFBmJU",
             "access": "eyJhbGciOiJIUzI1NiIsI...ODk2IiwidXNlcl9pZCI6MX0.h-ZFeFyttJs7dE7al6sDM_s8nAc-UbhLoXspeOMenpU"
         }

2. **POST jwt/refresh/?** [name='jwt-refresh'] - обновление jwt-токенов.

         запрос: refresh-токен, например:
      
         {
             "refresh": "eyJhbGciOiJIUzI1NiIsInR5c...IsInVzZXJfaWQiOjF9.kGVNN3IqgJ4GqFd5GqwVBF0tft6Zly9yt3n-o5qkf6o"
         }
      
         результат: access-токен, например:
      
         {
             "access": "eyJhbGciOiJIUzI1NiIsI...MWFhIiwidXNlcl9pZCI6MX0.6ypUqk65Uv9agbEq-KEaxu_7sdfKvvWHu5ccAcNGqs0"
         }

3. **POST jwt/verify/?** [name='jwt-verify'] - проверка jwt-токенов

         запрос: token (refresh или access), например:
         
         `{
             "token": "eyJhbGciOiJIUzI1NiIsIn...NGMwIiwidXNlcl9pZCI6MX0.sklX78DpWjrObyONhPDFmfIs5m1HMQhO3qmRxxtWKpY"
         }`
         
         результат: {} status 200 OK, если с токеном всё в порядке.

4. **POST auth/token/login/** - получение токена для аутентификации пользователя

         запрос: email, password
         
         результат: токен аутентификации, сохранённый в таблице БД, например:
         
         "auth_token": "5701c707af89400b6c0f49125514760c6d5cef81"

5. **POST auth/token/logout/** - выход пользователя с сайта

         запрос: headers: Authorization: Token 5701c707af89400b6c0f49125514760c6d5cef81 
         
         результат: токен аутентификации удаляется из таблицы БД, пользователь выходит с сайта, но не перестаёт быть активным.

6. **POST users/$** [name='users-list'] - CREATE - регистрация пользователя:

         запрос: email, password, re_password

         результат: регистрация пользователя, отправка письма со ссылкой для активации на почту (сейчас is_active=False)

7. **GET users/$** [name='users-list'] - READ - список профилей пользователя:

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4
         
         результат: данные профиля

8. **POST users/activation/$** [name='users-activation'] - активация пользователя по ссылке из письма:

         запрос: из ссылки в письме, например, http://127.0.0.1:8000/users/activation/Mw/c6wush-4a905a7e9c07811b3da72801dd7b5c88
         
         uid: Mw, token: c6wush-4a905a7e9c07811b3da72801dd7b5c88
         
         результат: пользователь активирован - is_active=True

   **Внимание!** После активации нужно сгенерировать токен для пользователя **'/auth/token/login/'**, иначе на следующих шагах авторизации вы получите ошибку.

9. **GET users/me/$** [name='users-me'] - READ - получение своего профиля:

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4 
         
         результат: данные профиля

10. **PUT, PATCH users/me/$** [name='users-me'] - UPDATE - полное (PUT) или частичное (PATCH) изменение своего профиля:

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4
      
         результат: изменение данных профиля (доступны для изменения все поля)

11. **GET users/{id}/$** [name='users-detail'] - READ - просмотр профиля пользователя (USER - свой, ADMIN - любой)

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4
         
         результат: данные отдельного пользователя

12. **PUT, PATCH users/{id}/$** [name='users-detail'] - UPDATE - полное (PUT) или частичное (PATCH) пользователя (USER - свой, ADMIN - любой)

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4
         
         результат: изменение данных профиля (доступны для изменения: "id", "email", first_name", "last_name", "phone", "role")

13. **DELETE users/{id}/$** [name='users-detail'] - DELETE - удаление пользователя (USER - свой, ADMIN - любой)

         запрос: email, password, headers: Authorization: Token 0944e011ae67e1a7e9b19a371d65af652b6d4fe4
         
         обязательно подтверждать (передавать) пароль удаляемого пользователя, например:
         
         {
             "current_password": "123qwe456asd"
         }
         
         результат: удаляет токен аутентификации и вызывает удаление для данного пользователя.

14. **POST users/resend_activation/$** [name='users-resend-activation'] - повторная отправка письма для активации:

         запрос: email 
         
         Внимание! Электронное письмо не будет отправлено, если пользователь уже активен или у него нет подходящего пароля
         
         результат: регистрация пользователя, отправка письма со ссылкой для активации на почту (если is_active=False), далее снова необходимо выполнить POST 'users/activation/$'

15. **POST users/set_password/$** [name='users-set-password'] - изменение пароля авторизованного пользователя (без оправки письма по почте):

         запрос: new_password, re_new_password, current_password, например:
         
         {
             "new_password": "123qwe456asd",
             "re_new_password": "123qwe456asd",
             "current_password": "456asd123qwe"
         }
         
         результат: изменение пароля авторизованного пользователя.

16. **POST users/reset_password/$** [name='users-reset-password'] - отправка пользователю электронного письма со ссылкой для сброса пароля.

         запрос: email
      
         результат: письмо, содержащее ссылку для изменения пароля

17. **POST users/reset_password_confirm/$** [name='users-reset-password-confirm']

         запрос: из ссылки в письме, например, http://127.0.0.1:8000/users/reset_password_confirm/Mg/c6x4t4-d99843627f3f67e169f997a8a9ef6670
         
         uid: Mg, token: c6x4t4-d99843627f3f67e169f997a8a9ef6670, new_password, например:
         
         {
             "uid": "Mg",
             "token": "c6x4t4-d99843627f3f67e169f997a8a9ef6670",
             "new_password": "456asd123qwe"
         }
         
         результат: изменён пароль пользователя

18. **POST users/set_email/$** [name='users-set-username'] - изменение адреса электронной почты авторизованного пользователя:

         запрос: current_password, new_email, re_new_email, например:
         
         {
             "current_password": "123qwe456asd",
             "new_email": "don.river.2024@yandex.ru",
             "re_new_email": "don.river.2024@yandex.ru"
         }
         
         результат: изменён адрес электронной почты на новый

19. **POST users/reset_email/$** [name='users-reset-username'] - отправка пользователю электронного письма со ссылкой для сброса имени пользователя (в данном случае - электронной почты, как поля для аутентификации)
   
         запрос: email
         
         результат: письмо, содержащее ссылку для изменения имени пользователя (в данном случае - электронной почты).
         
         Внимание! После изменения электронной почты снова нужно сгенерировать токен для пользователя '/auth/token/login/',
         иначе на следующем шаге вы получите ошибку.

20. **POST users/reset_email_confirm/$** [name='users-reset-username-confirm'] - завершение процесса сброса имени пользователя (в данном случае - электронной почты)

         запрос: из ссылки в письме, например, http://127.0.0.1:8000/users/reset_email_confirm/Mg/c6x6o3-805d9e302838140009ce608ad7b2b31d
      
         uid: Mg, token: c6x6o3-805d9e302838140009ce608ad7b2b31d, new_password, например:
      
         {
             "new_email": "zaberov.dv@gmail.com"
         }
         
         результат: изменено имя пользователя (электронной почты)