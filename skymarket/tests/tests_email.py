from rest_framework.test import APITestCase
from rest_framework import status


class EmailTestCase(APITestCase):

    def setUp(self):
        self.email = 'test_email@test.ru'

    def test_user_password_reset_email(self):
        """ Тестирование отправки электронного письма для восстановления пароля """

        data = {
            "email": self.email
        }
        response = self.client.post('/users/reset_password/', data=data)

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT,
        )
