from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    """Права создателя (владельца)"""
    message = 'Доступ запрещён, потому что Вы не являетесь владельцем.'

    def has_object_permission(self, request, view, obj):
        return request.user == obj.author


class IsAdmin(BasePermission):
    """Права администратора"""
    message = 'Действие доступно только администраторам проекта.'

    def has_permission(self, request, view):
        return request.user.role == 'admin'
