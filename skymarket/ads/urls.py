from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from ads.views.ad import AdSerializeCreateAPIView, AdSerializeRetrieveAPIView, AdSerializeListAPIView, \
    AdSerializeUpdateAPIView, AdSerializeDestroyAPIView
from ads.views.comment import ComSerializeCreateAPIView, ComSerializeListAPIView, ComSerializeRetrieveAPIView, \
    ComSerializeUpdateAPIView, ComSerializeDestroyAPIView

from ads.apps import SalesConfig

app_name = SalesConfig.name

urlpatterns = [
    # объявления
    path('ads/', AdSerializeListAPIView.as_view(), name='ads_all_list'),
    path('ads/me/', AdSerializeListAPIView.as_view(), name='ads_me_list'),
    path('ads/<int:pk>/', AdSerializeRetrieveAPIView.as_view(), name='ads_detail'),
    path('ads/create/', AdSerializeCreateAPIView.as_view(), name='ads_create'),
    path('ads/update/<int:pk>/', AdSerializeUpdateAPIView.as_view(), name='ads_update'),
    path('ads/delete/<int:pk>/', AdSerializeDestroyAPIView.as_view(), name='ads_delete'),
    # комментарии
    path('ads/<int:pk>/comments/', ComSerializeListAPIView.as_view(), name='comment_list'),
    path('ads/<int:pk>/comments/<int:commentId>/', ComSerializeRetrieveAPIView.as_view(), name='comment_detail'),
    path('ads/<int:pk>/comments/create/', ComSerializeCreateAPIView.as_view(), name='comment_create'),
    path('ads/<int:pk>/comments/update/<int:commentId>/', ComSerializeUpdateAPIView.as_view(), name='comment_update'),
    path('ads/<int:pk>/comments/delete/<int:commentId>/', ComSerializeDestroyAPIView.as_view(), name='comment_delete'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
