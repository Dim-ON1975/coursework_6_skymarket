from rest_framework import serializers


# DRF-spectacular сериализаторы
class CommonDetailSerializer(serializers.Serializer):
    """
    Сериализатор для DRF-spectacular документации.
    Используется для обработки кодов состояния.
    """
    detail = serializers.CharField()


class CommonDetailAndStatusSerializer(serializers.Serializer):
    """
    Сериализатор для DRF-spectacular документации.
    Используется для обработки кодов состояния.
    """
    status = serializers.IntegerField()
    details = serializers.CharField()
