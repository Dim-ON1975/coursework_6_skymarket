from django.contrib import admin

from ads.models import Ad, Comment
from django.utils.safestring import mark_safe


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    fields = ('author', 'title', 'description', 'price', 'image', 'ad_big_image', 'created_at',)
    readonly_fields = ('ad_image', 'ad_big_image',)
    list_display = ('id', 'author', 'title', 'description', 'ad_image', 'price', 'created_at',)
    list_filter = ('author', 'title', 'price', 'created_at',)
    search_fields = ('author', 'title', 'description', 'price',)
    save_on_top = True

    @admin.display(description='Изображение', ordering='content')
    def ad_image(self, ad: Ad):
        """
        Вычисляемое поле: отображение изображений в админ панели - маленькие изображения
        """
        if ad.image:
            return mark_safe(f'<div style="text-align: center; width: 60px; height: 60px; '
                             f'background-color: #DCDCDC; border: 1px solid #C0C0C0; overflow: hidden">'
                             f'<img src="{ad.image.url}" height=60 style:"margin: auto; display: block;"></div>')
        return 'Без фото'

    @admin.display(description='Изображение', ordering='content')
    def ad_big_image(self, ad: Ad):
        """
        Вычисляемое поле: отображение изображений в админ панели - большие изображения
        """
        if ad.image:
            return mark_safe(f'<p style="text-align: center"><img src="{ad.image.url}" height=250></p>')
        return 'Без фото'


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'ad', 'text', 'created_at',)
    list_filter = ('author', 'ad', 'created_at',)
    search_fields = ('author', 'title', 'text',)
