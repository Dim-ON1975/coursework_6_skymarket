from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse

from users.models import User
from ads.models import Ad, Comment


class CommentTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create(email='user@test.ru')
        self.ad = Ad.objects.create(
            title="title_comment",
            description="description_comment",
            price=1000,
            author=self.user
        )
        self.comment = Comment.objects.create(
            text="text_comment",
            author=self.user,
            ad=self.ad
        )

        # Аутентификация пользователя
        self.client.force_authenticate(user=self.user)

    def test_create_comment(self):
        """ Тестирование создания комментария """

        data = {
            "text": "text_comment"
        }

        comment_create_url = reverse('ads:comment_create', args=[self.ad.pk])
        response = self.client.post(comment_create_url, data=data)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED,
        )

        self.assertEqual(
            response.json().get('action'),
            data.get('action')
        )

        self.assertTrue(
            Comment.objects.get(pk=self.comment.pk).text,
            data.get('action')
        )

        # Проверяем наличие записи в базе данных
        self.assertTrue(
            Comment.objects.all().exists()
        )

        self.assertEqual(
            Comment.objects.get(pk=self.comment.pk).text,
            str(self.comment)
        )

    def test_list_comment(self):
        """ Тестирование чтения списка комментариев """

        comment_list_url = reverse('ads:comment_list', args=[self.ad.pk])

        response = self.client.get(comment_list_url)

        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )

        self.assertEqual(
            Comment.objects.get(pk=self.comment.pk).text,
            response.json().get('results')[0].get('text'))

    def test_retrieve_comment(self):
        """ Тест чтения одного комментария """

        comment_one_url = reverse('ads:comment_detail', args=[self.ad.pk, self.comment.pk])

        response = self.client.get(comment_one_url)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
        )

        response = response.json()

        self.assertEqual(response.get('author')['id'], self.user.pk)
        self.assertEqual(response.get('ad'), self.ad.pk)
        self.assertEqual(response.get('text'), 'text_comment')

    def test_update_comment(self):
        """ Тест редактирования комментария """

        data = {
            "text": "text_update_comment",
        }

        comment_update_url = reverse('ads:comment_update', args=[self.ad.pk, self.comment.pk])

        response = self.client.patch(comment_update_url, data)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
        )
        response = response.json()

        self.assertEqual(response.get('author')['id'], self.user.pk)
        self.assertEqual(response.get('ad'), self.ad.pk)
        self.assertEqual(response.get('text'), 'text_update_comment')

    def test_delete_comment(self):
        """ Тест удаления комментария """

        comment_delete_url = reverse('ads:comment_delete', args=[self.ad.pk, self.comment.pk])

        response = self.client.delete(comment_delete_url)

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT,
        )
        self.assertFalse(
            Comment.objects.all().exists(),
        )
